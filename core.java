import javax.security.auth.login.LoginException;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class DiscordBot extends ListenerAdapter {
    public static void main(String[] args) {
        try {
            JDA jda = JDABuilder.createDefault("YOUR_BOT_TOKEN_HERE").build();
            jda.addEventListener(new DiscordBot());
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;

        String message = event.getMessage().getContentRaw();

        if (message.equalsIgnoreCase("!hello")) {
            event.getChannel().sendMessage("Hello, willbot here!").queue();
        }
    }
}
