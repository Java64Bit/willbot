import discord4j.core.DiscordClient;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.reaction.ReactionEmoji;
import reactor.core.publisher.Mono;

public class willbotreactionrole {
    private static final String token = "token #";
    private static final long CHANNEL_ID = 1221293798370447373; // Channel ID where the message is
    private static final long MESSAGE_ID = 1221293798370447374; // ID of the message for reaction roles

    public static void main(String[] args) {
        DiscordClient client = DiscordClient.create(token);

        client.getEventDispatcher().on(ReactionAddEvent.class)
                .filter(event -> event.getMessageId().equals(MESSAGE_ID))
                .flatMap(willbotreactionrole::assignRole)
                .subscribe();

        client.login().block();
    }

    private static Mono<Void> assignRole(ReactionAddEvent event) {
        Message message = event.getMessage().block();
        if (message != null) {
            ReactionEmoji emoji = event.getEmoji();
            long roleId;
            switch (emoji.asUnicodeEmoji().orElse("")) {
                case "":
                    roleId = 1221656840434225192; // Role ID for the first emoji
                    break;
                case "":
                    roleId = 1221293988502442004; // Role ID for the second emoji
                    break;
                default:
                    return Mono.empty(); // Do nothing if the emoji doesn't match any roles
            }
            return event.getGuild().flatMap(guild -> guild.getRoleById(roleId))
                    .flatMap(role -> event.getMember().get().addRole(role.getId()));
        }
        return Mono.empty();
    }
}